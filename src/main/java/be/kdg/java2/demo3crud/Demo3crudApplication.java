package be.kdg.java2.demo3crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo3crudApplication {

    public static void main(String[] args) {
        SpringApplication.run(Demo3crudApplication.class, args);
    }

}
