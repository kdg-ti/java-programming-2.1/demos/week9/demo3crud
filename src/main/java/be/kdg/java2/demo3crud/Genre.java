package be.kdg.java2.demo3crud;

public enum Genre {
    THRILLER, BIOGRAPHY, FANTASY
}
