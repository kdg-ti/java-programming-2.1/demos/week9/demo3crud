package be.kdg.java2.demo3crud;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Stream;

@Component
public class DemoRunner implements CommandLineRunner {
    private Logger logger = LoggerFactory.getLogger(DemoRunner.class);
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public void run(String... args) throws Exception {
        Book book = new Book("Hitchhiker's Guide", 120,
                LocalDate.of(1973, 1, 1),
                LocalTime.NOON,
                Genre.FANTASY);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(book);
        logger.info("Created book:" + book);
        entityManager.getTransaction().commit();

        //add 100 books
        Stream.generate(Book::randomBook).limit(100).forEach(b -> {
            EntityManager em = entityManagerFactory.createEntityManager();
            em.getTransaction().begin();
            em.persist(b);
            em.getTransaction().commit();
            em.close();//closing is important!!!!
        });

        //query for books
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List<Book> books = em.createQuery("select b from Book b where b.title like 'title1%'").getResultList();
        em.getTransaction().commit();
        em.close();
        books.forEach(System.out::println);

        System.out.println("Query with parameter");
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        Query query = em.createQuery("select b from Book b where b.title = :title");
        query.setParameter("title","Hitchhiker's Guide");
        books = query.getResultList();
        em.getTransaction().commit();
        em.close();
        books.forEach(System.out::println);

        //find one book by id and change it
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        book = em.find(Book.class, 23);
        //book is still managed by the EnityManager!
        book.setGenre(Genre.FANTASY);
        em.getTransaction().commit();
        em.close();

        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.createQuery("update Book b set b.title = 'good book' where b.pages < 200").executeUpdate();
        em.getTransaction().commit();
        em.close();

        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        book = em.find(Book.class, 23);
        em.remove(book);
        em.getTransaction().commit();
        em.close();

        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        book = em.find(Book.class, 25);
        em.getTransaction().commit();
        em.close();
        //book is no longer managed by the EnityManager!
        //book.setTitle("great book");
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        //we attach it back to the em, it returns a managed
        book = em.merge(book);
        book.setTitle("great book");
        em.getTransaction().commit();
        em.close();
    }
}
